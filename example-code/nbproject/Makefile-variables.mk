#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug64bit configuration
CND_PLATFORM_Debug64bit=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug64bit=dist/Debug64bit/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug64bit=smoke
CND_ARTIFACT_PATH_Debug64bit=dist/Debug64bit/GNU-Linux-x86/smoke
CND_PACKAGE_DIR_Debug64bit=dist/Debug64bit/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug64bit=example-code.tar
CND_PACKAGE_PATH_Debug64bit=dist/Debug64bit/GNU-Linux-x86/package/example-code.tar
# Debug32bit configuration
CND_PLATFORM_Debug32bit=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug32bit=dist/Debug32bit/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug32bit=smoke
CND_ARTIFACT_PATH_Debug32bit=dist/Debug32bit/GNU-Linux-x86/smoke
CND_PACKAGE_DIR_Debug32bit=dist/Debug32bit/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug32bit=example-code.tar
CND_PACKAGE_PATH_Debug32bit=dist/Debug32bit/GNU-Linux-x86/package/example-code.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
