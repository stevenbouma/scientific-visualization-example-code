#include <string>
#include <GL/glut.h>
#include <sstream>
#include <iomanip>
#include "Utilities.h"

using namespace std;

int clamp(float x)
{ 
	return ((x)>=0.0?((int)(x)):(-((int)(1-(x))))); 
}

// clamp value between min and max
float clamp(float value, float min, float max)
{
	return value < min ? min : ( value > max ? max : value);
}

// text rendering
void renderString(float x, float y, string const &text, Color const &color)
{    
  glColor3f(color.r, color.g, color.b); 
  glRasterPos2f(x, y);
  for(size_t chindex = 0; chindex < text.length(); chindex++)
	  glutBitmapCharacter(GLUT_BITMAP_HELVETICA_10, text[chindex]);  
}

string tostring(float value)
{
	stringstream stringconvert;
	stringconvert << setprecision(2) << value;	
	return stringconvert.str();
}