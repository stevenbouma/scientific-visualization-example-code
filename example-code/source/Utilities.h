#ifndef UTILITIES_H_INCLUDED
#define	UTILITIES_H_INCLUDED

#include <string>
#include "Color.h"
using namespace std;

// some utility functions

int clamp(float x);
float clamp(float value, float min, float max);
void renderString(float x, float y, string const &text, Color const &color);
string tostring(float value);

#endif

