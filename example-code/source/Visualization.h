#ifndef VISUALIZATION_H_INCLUDED
#define	VISUALIZATION_H_INCLUDED

#include "Colormap.h"

class Simulation;

class Visualization
{
	friend class Application;
	
	public:
		
		Visualization();
		void visualize(Simulation const &simulation, int winWidth, int winHeight);
		
		//different types of color mapping
		enum ColorMode
		{						
			Rainbow,
			Yellow,
			Fire,
			Tropical,
			ColorModeCount				// (automatically assigned)
		};
		
		enum Option
		{
			DrawSmoke,				// draw the smoke or not
			DrawVectorField,		// draw the vector field or not
			UseDirectionColoring,	// use direction color-coding or not
			SmoothShading,
			ClampData,
			OptionsCount			// (automatically assigned)
		};
		
		enum DataSet
		{
			Density,			
			Velocity,
			Force,
			DataSetCount
		};
		
		void toggle(Option option);
		void enable(Option option);
		void disable(Option option);
		bool is_enabled(Option option) const;		
		void scale_hedgehogs(float scale);
		float hedgehog_scale() const;
		
		void rainbow(float value, float* R, float* G, float* B);
		void set_colormap(Simulation const &simulation, size_t index1D);
		void direction_to_color(float x, float y, int method);
		
		void init_colormaps();
		void display_colormap(float xpos, float ypos, float barwidth, float barheight);
		
	private:
		
		float vec_scale;				// scaling of hedgehogs 
		int options[OptionsCount];		// options boolean array
		ColorMode scalar_col;			//method for scalar coloring
		
		int selected_dataset;
		int selected_colormap;
		
		RangedColormap rainbow_colormap;
		RangedColormap tropical_colormap;
		RangedColormap fire_colormap;
		BasicColormap grey_to_yellow_map;
};

#endif