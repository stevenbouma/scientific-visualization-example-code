#include <algorithm>
#include <cmath>
#include <rfftw.h>              //the numerical simulation FFTW library
#include <GL/glut.h>

#include "Visualization.h"
#include "Simulation.h"
#include "Utilities.h"
#include "Colormap.h"
#include "Vector.h"

using namespace std;

Visualization::Visualization()
{
	vec_scale = 1000;
	scalar_col = Rainbow;
	options[UseDirectionColoring] = false;
	options[DrawSmoke] = true;
	options[DrawVectorField] = false;
	options[SmoothShading] = true;
	selected_colormap = Fire;
	selected_dataset = Density;
	init_colormaps();	
}

void Visualization::init_colormaps()
{	
	// some predefined colors
	Color darkgrey(0.2, 0.2, 0.2);
	Color yellow(1.0, 0.9, 0.0);
	Color white(1.0, 1.0, 1.0);
	Color red(0.9, 0.0, 0.0);
	
//	darkgrey = darkgrey.toHSV();
//	yellow = yellow.toHSV();
//	white = white.toHSV();
//	red = red.toHSV();
	
	// tropical	
	Color tropical8(93 / 255.0, 128 / 255.0, 0.0);			// greenish
	Color tropical7(160 / 255.0, 250 / 255.0, 130 / 255.0); // baby blue / cyan
	Color tropical6(123 / 255.0, 90 / 255.0, 254);			// purple
	Color tropical5(134 / 255.0, 80 / 255.0, 76 / 255.0);	// brown
	Color tropical4(240 / 255.0, 60 / 255.0, 35 / 255.0);	// light red	
	Color tropical3(236 / 255.0, 186 / 255.0, 35 / 255.0);	// orangeish	
	Color tropical2(131 / 255.0, 119 / 255.0, 13 / 255.0);	// green lighter
	Color tropical1(10 / 255.0, 40 / 255.0, 5 / 255.0);		// dark green		
	
	// init basic colormap
	
	vector<Color> grey_to_yellow;
	for(size_t index = 0; index < 256; ++index)	
		grey_to_yellow.push_back(Color::interpolate(darkgrey, yellow, index / 255.0));
	
	grey_to_yellow_map  = BasicColormap(grey_to_yellow, Range(0.0, 10.0));	
					
	// map ranges to colors
	
	// fire
	RangedColormap::RangeMap fire;
	fire[Range(0.0, 0.5)] = make_pair(darkgrey, red);
	fire[Range(0.5, 0.9999)] = make_pair(red, yellow);
	fire[Range(0.9999, 2.0)] = make_pair(yellow, white);
	fire[Range(2.0, 11.0)] = make_pair(white, white);
	
	// tropical
	RangedColormap::RangeMap tropical;
	tropical[Range(0.0, 0.3)] =  make_pair(tropical1, tropical2);
	tropical[Range(0.3, 0.4)] = make_pair(tropical2, tropical3);
	tropical[Range(0.4, 0.55)] = make_pair(tropical3, tropical4);
	tropical[Range(0.55, 0.6)] = make_pair(tropical4, tropical5);
	tropical[Range(0.6, 0.65)] = make_pair(tropical5, tropical6);
	tropical[Range(0.65, 0.9999)] = make_pair(tropical6, tropical7);
	tropical[Range(0.9999, 11.0)] = make_pair(tropical7, tropical8);
	
	tropical_colormap = RangedColormap(tropical);
	fire_colormap = RangedColormap(fire);
}

void Visualization::toggle(Option option)
{
	options[option] = !options[option];
}

void Visualization::enable(Option option)
{
	options[option] = true;
}

void Visualization::disable(Option option)
{
	options[option] = false;
}

bool Visualization::is_enabled(Option option) const
{
	return options[option];
}

void Visualization::scale_hedgehogs(float scale)
{
	vec_scale *= scale;
}

float Visualization::hedgehog_scale() const
{
	return vec_scale;
}

//rainbow: Implements a color palette, mapping the scalar 'value' to a rainbow color RGB
void Visualization::rainbow(float value, float* R, float* G, float* B)
{
	const float dx=0.8;
	if (value<0) value=0; if (value>1) value=1;
	value = (6-2*dx)*value+dx;	
	*R = max(0.0, (3-fabs(value-4)-fabs(value-5)) / 2.0);
	*G = max(0.0, (4-fabs(value-2)-fabs(value-4)) / 2.0);
	*B = max(0.0, (3-fabs(value-1)-fabs(value-2)) / 2.0);
}

//set_colormap: Sets three different types of colormaps
void Visualization::set_colormap(Simulation const &simulation, size_t index1D)
{
	float value = 0.0;	
	
	switch(selected_dataset)
	{
		case Density : 
		{ 
			value = simulation.rho[index1D]; 
		} break;
		
		case Velocity : 
		{ 
			Vector2<float> velocity(simulation.vx[index1D], simulation.vy[index1D]);
			value = velocity.length() * 10.0;	// scale factor here
		} break;
		
		case Force : 
		{ 
			Vector2<float> force(simulation.fx[index1D], simulation.fy[index1D]);
			value = force.length() * 10.0;	// scale factor here
		} break;
	
		default : { value = simulation.rho[index1D]; }
	}		
	
	switch(selected_colormap)
	{
		case Rainbow : { rainbow_colormap.glColor(value); } break;
		case Fire : { fire_colormap.glColor(value); } break;
		case Yellow : { grey_to_yellow_map.glColor(value, options[ClampData]); } break;
		case Tropical : { tropical_colormap.glColor(value); } break;
		default: {}
	}		
}

// render selected colormap to the side
void Visualization::display_colormap(float xpos, float ypos, float barwidth, float barheight)
{		
	switch(selected_colormap)
	{
		case Rainbow : { rainbow_colormap.display(xpos, ypos, barwidth, barheight); } break;
		case Fire : { fire_colormap.display(xpos, ypos, barwidth, barheight); } break;
		case Yellow : {grey_to_yellow_map.display(xpos, ypos, barwidth, barheight); } break;
		case Tropical : { tropical_colormap.display(xpos, ypos, barwidth, barheight); } break;
		default: {}
	}		
}

//direction_to_color: Set the current color by mapping a direction vector (x,y), using
//                    the color mapping method 'method'. If method==1, map the vector direction
//                    using a rainbow colormap. If method==0, simply use the white color
void Visualization::direction_to_color(float x, float y, int method)
{
	float r,g,b,f;
	if (method)
	{
	  f = atan2(y,x) / 3.1415927 + 1;
	  r = f;
	  if(r > 1) r = 2 - r;
	  g = f + .66667;
      if(g > 2) g -= 2;
	  if(g > 1) g = 2 - g;
	  b = f + 2 * .66667;
	  if(b > 2) b -= 2;
	  if(b > 1) b = 2 - b;
	}
	else
	{ r = g = b = 1; }
	glColor3f(r,g,b);
}
	
//visualize: This is the main visualization function
void Visualization::visualize(Simulation const &simulation, int winWidth, int winHeight)
{	
	glShadeModel((options[SmoothShading] ? GL_SMOOTH : GL_FLAT));			
	int idx;
	double px, py;
	const int DIM = Simulation::DIM;
	const float ColormapWidth = 50.0;
	float render_area_w = static_cast<float>(winWidth) - ColormapWidth;
	float render_area_h = static_cast<float>(winHeight);	
	float wn = render_area_w / static_cast<float>(DIM + 1);  // Grid cell width
	float hn = render_area_h / static_cast<float>(DIM + 1);  // Grid cell heigh
				
	if (options[DrawSmoke])
	{
		glPolygonMode(GL_FRONT, GL_FILL);
		for (int j = 0; j < DIM - 1; j++)			//draw smoke
		{
			glBegin(GL_TRIANGLE_STRIP);

			px = wn;
			py = hn + (fftw_real)j * hn;
			idx = (j * DIM);

			set_colormap(simulation, idx);
			glVertex2f(px,py);

			for (int i = 0; i < DIM - 1; i++)
			{
				px = wn + (fftw_real)i * wn;
				py = hn + (fftw_real)(j + 1) * hn;
				idx = ((j + 1) * DIM) + i;
				set_colormap(simulation, idx);
				glVertex2f(px, py);
				px = wn + (fftw_real)(i + 1) * wn;
				py = hn + (fftw_real)j * hn;
				idx = (j * DIM) + (i + 1);
				set_colormap(simulation, idx);
				glVertex2f(px, py);
			}

			px = wn + (fftw_real)(DIM - 1) * wn;
			py = hn + (fftw_real)(j + 1) * hn;
			idx = ((j + 1) * DIM) + (DIM - 1);
			set_colormap(simulation, idx);
			glVertex2f(px, py);
			glEnd();
		}
	}

	if (options[DrawVectorField])
	{
		glBegin(GL_LINES);				//draw velocities
		for (int i = 0; i < DIM; i++)
		{
			for (int j = 0; j < DIM; j++)
			{
				idx = (j * DIM) + i;
				direction_to_color(simulation.vx[idx], simulation.vy[idx], options[UseDirectionColoring]);
				glVertex2f(wn + (fftw_real)i * wn, hn + (fftw_real)j * hn);
				glVertex2f((wn + (fftw_real)i * wn) + vec_scale * simulation.vx[idx], (hn + (fftw_real)j * hn) + vec_scale * simulation.vy[idx]);
			}
		}
		glEnd();
	}
	
	// render colormap + legend
	float xpos = winWidth - ColormapWidth - 5.0;
	float ypos = 10.0;
	float barwidth = ColormapWidth - 25.0;
	float barheigth = winHeight - 20.0;
	display_colormap(xpos, ypos, barwidth, barheigth);
}


