#include "Colormap.h"
#include <GL/gl.h>
#include <algorithm>
#include <cmath>
#include <utility>
#include <vector>
#include <string>
#include "Utilities.h"

using namespace std;

BasicColormap::BasicColormap()
:	
	d_input_range(0.0, 1.0)		
{	
	d_colors.push_back(Color(1.0, 1.0, 1.0));
}

BasicColormap::BasicColormap(vector<Color> const &colors, Range const &range)
:
	d_colors(colors),
	d_input_range(range)
{	
}

const Range &BasicColormap::inputRange() const
{
	return d_input_range;
}

Color const &BasicColormap::color(float scalar, bool use_clamp) const
{
	float x = (scalar - d_input_range.min()) / d_input_range.max();
	
	if(use_clamp)
		x = clamp(scalar, 0.0, 1.0);
	
	size_t index = static_cast<size_t>(x * (d_colors.size() - 1));	
	return d_colors[index];
}

void BasicColormap::glColor(float scalar, bool use_clamp) const
{
	Color const &col = color(scalar, use_clamp);
	glColor3f(col.r, col.g, col.b);
}

// draw a basic colormap at (x,y), and render a legend beside it
void BasicColormap::display(float x, float y, float width, float height) const
{
	float step = height / d_colors.size();
	
	glBegin(GL_QUAD_STRIP);
	
	for(size_t index = 0; index < d_colors.size(); ++index)
	{		
		glColor3f(d_colors[index].r, d_colors[index].g, d_colors[index].b);		
		glVertex2f(x, y + index * step);
		glVertex2f(x + width, y + index * step);		
	}
	
	glEnd();
	
	renderString(x + width + 5.0, y, "0.0", d_colors[0]);
	renderString(x + width + 5.0, y + height - 10.0, "1.0", d_colors.back());	
}

RangedColormap::RangedColormap()
:
	num_entries(0)
{	
}

RangedColormap::RangedColormap(RangeMap const &rm)
:
	range_map(rm)
{	
	num_entries = rm.size();
}

Color RangedColormap::color(float value) const
{			
	// find the range and colors that the scalar value is associated with
	RangeMap::const_iterator it = range_map.find(value);
	
	if(it == range_map.end())
		return Color(0.0, 0.0, 0.0);	// check your map entries
		
	Range const &range = (*it).first;		
	pair<Color, Color> const &colors = (*it).second;		
	
	return Color::interpolate(colors.first, colors.second, (value - range.min()) / range.max());
}

void RangedColormap::glColor(float scalar) const
{
	Color col = color(scalar);
	glColor3f(col.r, col.g, col.b);
}

void RangedColormap::display(float x, float y, float width, float height) const
{
	float step = height / num_entries;
				
	size_t index = 0;
	
	// iterate over the map, iterator->first = key and iterator->second = value
	for(RangedColormap::RangeMap::const_iterator iter = range_map.begin(); iter != range_map.end(); iter++) 
	{
		Range const &range = (*iter).first;		
		pair<Color, Color> const &colors = (*iter).second;
		Color const& color1 = colors.first;
		Color const& color2 = colors.second;
		
		glBegin(GL_QUADS);				
			glColor3f(color1.r, color1.g, color1.b);
			glVertex2f(x, y + index * step);
			glVertex2f(x + width, y + index * step);			
			glColor3f(color2.r, color2.g, color2.b);			
			glVertex2f(x + width, y + index * step + step);
			glVertex2f(x, y + index * step + step);	
		glEnd();							
		
		// display legend value (minimum of range)
		renderString(x + width + 5.0, y + index * step, tostring(range.min()), color1);		
		
		if(index == num_entries - 1)	// last one	too (max of range)
			renderString(x + width + 5.0, y + index * step + step - 5.0, tostring(range.max()), color2);		
		
		index++;
	}		
}