#include "Range.h"

Range::Range(float min, float max)
:
	d_min(min),
	d_max(max)
{
};

Range::Range(Range const &other)
:
	d_min(other.d_min),
	d_max(other.d_max)
{
};	

Range::Range(float val)
:
	d_min(val),
	d_max(val)
{
};

float Range::min() const
{
	return d_min;
}

float Range::max() const
{
	return d_max;
}

// 1 if this > other
// 0 if this == other
//-1 if this < other
int Range::compare(const Range &other) const
{
	if (other.d_min == other.d_max && d_min == d_max)
		return (d_min > other.d_min) ? 1 : ((d_min == other.d_min) ? 0 : -1);		

	else if (other.d_max == other.d_min)
		return (d_min > other.d_min) ? 1 : ((d_min <= other.d_min && d_max > other.d_min) ? 0 : -1);

	else if (d_min == d_max)
		return (d_min >= other.d_max) ? 1 : ( (d_min < other.d_max && d_min >= other.d_min) ? 0 : -1);
	
	if (other.d_min == d_min && other.d_max == d_max)	// equal
		return 0;	
	else if (other.d_min < d_min && other.d_max <= d_min)	// this > other
		return 1;
	else if (other.d_min > d_min && other.d_min >= d_max)	// this < other
		return -1;		
};

bool Range::operator== (Range const &other ) const 
{
	return compare(other) == 0; 
};

bool Range::operator!= (Range const &other ) const 
{
	return compare(other) != 0; 
};

bool Range::operator< (Range const &other ) const 
{
	return compare(other) < 0; 
};

bool Range::operator> (Range const &other ) const 
{
	return compare(other) > 0; 
};

bool Range::operator<= (Range const &other ) const 
{
	return compare(other) <= 0; 
};

bool Range::operator>= (Range const &other ) const 
{
	return compare(other) >= 0; 
};