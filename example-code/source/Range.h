#ifndef RANGE_INCLUDED
#define	RANGE_INCLUDED

// basic class to represent a range (x..y], is "comparable"
class Range
{
	public:

		Range(float min, float max);
		Range(Range const &other);
		Range(float val);

		float min() const;
		float max() const;
		
		int compare(const Range &other) const;

		bool operator== (Range const &other) const;
		bool operator!= (Range const &other) const;
		bool operator< (Range const &other) const;
		bool operator> (Range const &other) const;
		bool operator<= (Range const &other) const;
		bool operator>= (Range const &other) const;
		
	private:
		
		float d_min;
		float d_max;
};

#endif
