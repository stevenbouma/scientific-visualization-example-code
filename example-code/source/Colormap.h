#ifndef COLORMAP_INCLUDED
#define	COLORMAP_INCLUDED

#include <map>
#include <vector>
#include "Color.h"
#include "Range.h"

using namespace std;

// represent the colormap as a vector of colors (e.g. 256 different colors)
// map an input value to one of the colors based on the known "input range"
class BasicColormap
{
	public:
		BasicColormap();
		BasicColormap(vector<Color> const &colors, Range const &range);
		const Range &inputRange() const;
		void display(float x, float y, float width, float height) const;
		Color const &color(float scalar, bool use_clamp) const;
		void glColor(float scalar, bool use_clamp) const;
		
	private:
		vector<Color> d_colors;
		Range d_input_range;	
};

// in this datastructure a range of values (e.g. 0.1 .. 0.5), 
// maps to a pair of colors (e.g. red, green)
// the map can be queried using a scalar, and will return 
// two colors for interpolation
	
class RangedColormap
{			
	public:
		
		typedef map<Range, pair<Color, Color> > RangeMap;
		
		RangedColormap();
		RangedColormap(RangeMap const &rangemap);
		void display(float x, float y, float width, float height) const;
		Color color(float scalar) const;
		void glColor(float scalar) const;
		
	private:
		size_t num_entries;
		RangeMap range_map;
};

#endif

