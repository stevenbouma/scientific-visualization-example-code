// Usage: Drag with the mouse to add smoke to the fluid. This will also move a "rotor" that disturbs
//        the velocity field at the mouse location. Press the indicated keys to change options
//--------------------------------------------------------------------------------------------------

#include <iostream>
#include "Application.h"
#include "Colormap.h"

using namespace std;

int main(int argc, char** argv)
{	
	// tropical	
//	Color tropical8(93 / 255.0, 128 / 255.0, 0.0); // greenish
//	Color tropical7(160 / 255.0, 250 / 255.0, 130 / 255.0); // baby blue / cyan
//	Color tropical6(123 / 255.0, 90 / 255.0, 254); // purple
//	Color tropical5(134 / 255.0, 80 / 255.0, 76 / 255.0); // brown
//	Color tropical4(240 / 255.0, 60 / 255.0, 35 / 255.0); // light red	
//	Color tropical3(236 / 255.0, 186 / 255.0, 35 / 255.0); // orangeish	
//	Color tropical2(131 / 255.0, 119 / 255.0, 13 / 255.0); // green lighter
//	Color tropical1(10 / 255.0, 40 / 255.0, 5 / 255.0); // dark green			
//	
//	map<Range, pair<Color, Color> > range_map;
//	range_map[Range(0.0, 0.3)] = make_pair(tropical1, tropical2);
//	range_map[Range(0.3, 0.4)] = make_pair(tropical2, tropical3);
//	range_map[Range(0.4, 0.55)] = make_pair(tropical3, tropical4);
//	range_map[Range(0.55, 0.6)] = make_pair(tropical4, tropical5);
//	range_map[Range(0.6, 0.65)] = make_pair(tropical5, tropical6);
//	range_map[Range(0.65, 1.99)] = make_pair(tropical6, tropical7);	
//	
//	
//	cout << range_map[0.2].first << '\n';
//	cout << range_map[0.5].first << '\n';
//	
	
	
	
	try
	{		
		Application::outputUsage();
		Application::initialize(&argc, argv);
	}
	catch(exception &ex)
	{
		cerr << "an exception occurred: " << ex.what() << '\n';		
	}
	catch(char const *description)
	{
		cerr << "an exception occurred: " << description << '\n';		
	}	
	catch(...)
	{
		cerr << "an exception occurred.\n";		
	}
}