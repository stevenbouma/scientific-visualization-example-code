#ifndef COLOR_INCLUDED
#define	COLOR_INCLUDED

#include "Vector.h"

class Color : public Vector3<float>
{
	public:
		Color(float r, float g, float b);
		Color();
		
		Color toHSV() const;
		Color toRGB() const;
		static Color interpolate(Color const &first, Color const &second, float x);
};

#endif
