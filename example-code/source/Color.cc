#include "Color.h"
#include "Utilities.h"
#include <algorithm>

Color::Color(float r, float g, float b) 
: 
	Vector3<float>(r, g, b)
{
}

Color::Color() 
: 
	Vector3<float>(0.0, 0.0, 0.0) 
{
}

Color Color::toHSV() const
{
	Color hsv;
	float M = max(r, max(g, b));
	float m = min(r, max(g, b));
	float d = M - m;
	
	hsv.v = M;								// value
	hsv.s = (d > 0.00001) ? d / M : 0.0;	// saturation
	if (hsv.s == 0.0)
		hsv.h = 0.0;
	else
	{		
		if (r == M) 
			hsv.h = (g - b) / d;
		else if (g == M) 
			hsv.h = 2.0 + (b - r) / d;
		else
			hsv.h = 4.0 + (r - g) / d;
		
		hsv.h = hsv.h / 6.0;
		if (hsv.h < 0.0)
			hsv.h = hsv.h + 1.0;
	}		
	
	return hsv;
}

Color Color::toRGB() const
{
	Color rgb;
	
	int hue_case = static_cast<int>(h * 6.0);
	float frac = 6.0 * h - hue_case;
	float lx = v * (1.0 - s);
	float ly = v * (1.0 - s * frac);
	float lz = v * (1.0 - s * (1.0 - frac));
	
	switch (hue_case)
	{
		case 0:
		case 6: { rgb = Color(v, lz, lx); } break;
		case 1: { rgb = Color(ly, v, lx); } break;
		case 2: { rgb = Color(lx, v, lz); } break;		
		case 3: { rgb = Color(lx, ly, v); } break;
		case 4: { rgb = Color(lz, lx, v); } break;
		case 5: { rgb = Color(v, lx, ly); } break;		
	}
	
	return rgb;
}


Color Color::interpolate(Color const &first, Color const &second, float x)
{
	clamp(x, 0.0, 1.0);
	float r = first.r * (1.0 - x) + second.r * x;
	float g = first.g * (1.0 - x) + second.g * x;
	float b = first.b * (1.0 - x) + second.b * x;
	return Color(r, g, b);
}